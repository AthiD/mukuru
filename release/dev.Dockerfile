FROM golang:1.17 

WORKDIR /app

# Copy the source code from the src directory into the Docker image
COPY src/ .

RUN go mod init main

RUN CGO_ENABLED=0 GOOS=linux go build -o main .

EXPOSE 3000

CMD ["./main"]
